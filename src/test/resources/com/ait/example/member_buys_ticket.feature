Feature: Member buys ticket
  In order to watch some great theatre
  As a theatre member
  I want to buy a ticket to a Show

  Scenario: No tickets bought last season and anyone can buy tickets to the show
    Given I bought 0 tickets last season
     And anyone can buy tickets for a Level "C" show
     And the show "Swan lake" is level "C" show
    When I try to buy a ticket for the show called Swan lake
    Then I should be able to buy the ticket

   
  
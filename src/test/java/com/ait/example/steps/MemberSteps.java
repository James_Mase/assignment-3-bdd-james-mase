package com.ait.example.steps;

import com.ait.example.Member;
import com.ait.example.MemberRepository;

import cucumber.annotation.en.Given;
import org.springframework.beans.factory.annotation.Autowired;


public class MemberSteps {

    @Autowired
    private MemberRepository memberRepository;

    @Given("^I bought (\\d+) tickets last season$")  
    public void givenThatIBoughtTicketsLastSeason(int numberOfTickets) {
        Member member = new Member();
        member.setNumberOfTicketsBoughtLastSeason(numberOfTickets);
        memberRepository.store(member);
    }

}

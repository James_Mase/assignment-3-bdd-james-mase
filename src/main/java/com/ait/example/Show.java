package com.ait.example;

public class Show {

    private String name;

    private Showlevel level;

    public Show(String name, Showlevel category) {
        this.name = name;
        this.level = category;
    }

    public String getName() {
        return name;
    }

    public Showlevel getLevel() {
        return level;
    }
}
